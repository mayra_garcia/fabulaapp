package com.example.registro

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_fabula4.*

class Fabula4 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fabula4)

        val bundle : Bundle? = intent.extras

        bundle?.let {bundleLibriDeNull ->
            val nombre = bundleLibriDeNull.getString("nombre","Desconocido")
            val edad = bundleLibriDeNull.getString("edad","0")

            tvNombre4.text =  nombre
            tvEdad4.text =  edad
        }

        btnAvanzar4.setOnClickListener {

            //Pasar datos de la activity Fabula4 a la activity Fabula5
            val nombre = tvNombre4.text.toString()
            val edad = tvEdad4.text.toString()

            val bundle = Bundle()
            bundle.apply {
                putString("nombre",nombre)
                putString("edad",edad)
            }

            tvNombre4.text = ""
            tvEdad4.text = ""

            val intent = Intent(this,Fabula5::class.java).apply {
                putExtras(bundle)
                finish()
            }
            startActivity(intent)
        }

        btnRegresar4.setOnClickListener {

            //Pasar datos de la activity Fabula4 a la activity Fabula3
            val nombre = tvNombre4.text.toString()
            val edad = tvEdad4.text.toString()

            val bundle = Bundle()
            bundle.apply {
                putString("nombre",nombre)
                putString("edad",edad)
            }

            tvNombre4.text = ""
            tvEdad4.text = ""

            val intent = Intent(this,Fabula3::class.java).apply {
                putExtras(bundle)
                finish()
            }
            startActivity(intent)
        }
    }
}
