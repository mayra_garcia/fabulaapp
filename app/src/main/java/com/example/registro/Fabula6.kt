package com.example.registro

import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.airbnb.lottie.LottieAnimationView
import kotlinx.android.synthetic.main.activity_fabula6.*

class Fabula6 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fabula6)

        val bundle : Bundle? = intent.extras

        bundle?.let {bundleLibriDeNull ->
            val nombre = bundleLibriDeNull.getString("nombre","Desconocido")
            val edad = bundleLibriDeNull.getString("edad","0")

            tvNombre6.text =  nombre
            tvEdad6.text =  edad
        }

        btnAvanzar6.setOnClickListener {
            mostrarMoraleja()
        }

        btnRegresar6.setOnClickListener {

            //Pasar datos de la activity Fabula6 a la activity Fabula5
            val nombre = tvNombre6.text.toString()
            val edad = tvEdad6.text.toString()

            val bundle = Bundle()
            bundle.apply {
                putString("nombre",nombre)
                putString("edad",edad)
            }

            tvNombre6.text = ""
            tvEdad6.text = ""

            val intent = Intent(this,Fabula5::class.java).apply {
                putExtras(bundle)
                finish()
            }
            startActivity(intent)
        }
    }

    private fun mostrarMoraleja() {

        val builder: AlertDialog.Builder? = this.let {
            AlertDialog.Builder(it)
        }

        // Para poner el layaout perzonalizado en el dialog
        var view: View = layoutInflater.inflate(R.layout.moraleja_dialog, null)
        var moraleja: LottieAnimationView = view.findViewById(R.id.animation_view)
               builder?.setCancelable(false)
        builder?.setView(view)

        moraleja.repeatCount = 1
        moraleja.playAnimation()

        builder?.apply {
            setPositiveButton("Salir",
                DialogInterface.OnClickListener { dialog, id ->
                    finish()
                    startActivity(Intent(this@Fabula6,MainActivity::class.java))
                })
        }

        val dialog: AlertDialog? = builder?.create()

        dialog?.show()
    }
}
