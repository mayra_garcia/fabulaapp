package com.example.registro

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_fabula5.*

class Fabula5 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fabula5)

        val bundle : Bundle? = intent.extras

        bundle?.let {bundleLibriDeNull ->
            val nombre = bundleLibriDeNull.getString("nombre","Desconocido")
            val edad = bundleLibriDeNull.getString("edad","0")

            tvNombre5.text =  nombre
            tvEdad5.text =  edad
        }

        btnAvanzar5.setOnClickListener {

            //Pasar datos de la activity Fabula5 a la activity Fabula6
            val nombre = tvNombre5.text.toString()
            val edad = tvEdad5.text.toString()

            val bundle = Bundle()
            bundle.apply {
                putString("nombre",nombre)
                putString("edad",edad)
            }

            tvNombre5.text = ""
            tvEdad5.text = ""

            val intent = Intent(this,Fabula6::class.java).apply {
                putExtras(bundle)
                finish()
            }
            startActivity(intent)
        }

        btnRegresar5.setOnClickListener {

            //Pasar datos de la activity Fabula5 a la activity Fabula4
            val nombre = tvNombre5.text.toString()
            val edad = tvEdad5.text.toString()

            val bundle = Bundle()
            bundle.apply {
                putString("nombre",nombre)
                putString("edad",edad)
            }

            tvNombre5.text = ""
            tvEdad5.text = ""

            val intent = Intent(this,Fabula4::class.java).apply {
                putExtras(bundle)
                finish()
            }
            startActivity(intent)
        }
    }
}
