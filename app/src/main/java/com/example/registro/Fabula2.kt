package com.example.registro

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_fabula2.*

class Fabula2 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fabula2)

        val bundle : Bundle? = intent.extras

        bundle?.let {bundleLibriDeNull ->
            val nombre = bundleLibriDeNull.getString("nombre","Desconocido")
            val edad = bundleLibriDeNull.getString("edad","0")

            tvNombre2.text = nombre
            tvEdad2.text = edad
        }

        btnRegresar2.setOnClickListener {

            //Pasar datos de la activity Fabula2 a la activity Fabula1
            val nombre = tvNombre2.text.toString()
            val edad = tvEdad2.text.toString()

            val bundle = Bundle()
            bundle.apply {
                putString("nombre",nombre)
                putString("edad",edad)
            }

            tvNombre2.text = ""
            tvEdad2.text = ""

            val intent = Intent(this,Fabula1::class.java).apply {
                putExtras(bundle)
                finish()
            }
            startActivity(intent)
        }

        btnAvanzar2.setOnClickListener {

            //Pasar datos de la activity Fabula2 a la activity Fabula3
            val nombre = tvNombre2.text.toString()
            val edad = tvEdad2.text.toString()

            val bundle = Bundle()
            bundle.apply {
                putString("nombre",nombre)
                putString("edad",edad)
            }

            tvNombre2.text = ""
            tvEdad2.text = ""

            val intent = Intent(this,Fabula3::class.java).apply {
                putExtras(bundle)
                finish()
            }
            startActivity(intent)
        }
    }
}
