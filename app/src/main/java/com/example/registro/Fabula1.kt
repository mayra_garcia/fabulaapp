package com.example.registro

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_fabula1.*

class Fabula1 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fabula1)

        val bundle : Bundle? = intent.extras

        bundle?.let {bundleLibriDeNull ->
            val nombre = bundleLibriDeNull.getString("nombre","Desconocido")
            val edad = bundleLibriDeNull.getString("edad","0")

            tvNombre.text =  nombre
            tvEdad.text =  edad
        }

        btnAvanzar.setOnClickListener {

            //Pasar datos de la activity Fabula1 a la activity Fabula2
            val nombre = tvNombre.text.toString()
            val edad = tvEdad.text.toString()

            val bundle = Bundle()
            bundle.apply {
                putString("nombre",nombre)
                putString("edad",edad)
            }

            tvNombre.text = ""
            tvEdad.text = ""

            val intent = Intent(this,Fabula2::class.java).apply {
                putExtras(bundle)
                finish()
            }
            startActivity(intent)
        }

    }

    /*private fun mostrarMoraleja() {
        // 1. Instantiate an <code><a href="/reference/android/app/AlertDialog.Builder.html">AlertDialog.Builder</a></code> with its constructor
        val builder: AlertDialog.Builder? = this.let {
            AlertDialog.Builder(it)
        }

        // Para poner el layaout perzonalizado en el dialog
        var view: View = layoutInflater.inflate(R.layout.moraleja_dialog, null)
        var moraleja: LottieAnimationView = view.findViewById(R.id.animation_view)

        //builder?.setTitle("*** MORALEJA ***")
        builder?.setCancelable(false)
        builder?.setView(view)

        moraleja.repeatCount = 1
        moraleja.playAnimation()

        builder?.apply {
            setPositiveButton("Salir",
                DialogInterface.OnClickListener { dialog, id ->
                    finish()
                    //startActivity(Intent(this@Fabula1,MainActivity::class.java))
                })
        }

        val dialog: AlertDialog? = builder?.create()

        dialog?.show()
    }*/
}
