package com.example.registro

import android.content.DialogInterface
import android.content.Intent
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.airbnb.lottie.LottieAnimationView
import kotlinx.android.synthetic.main.activity_fabula1.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        Thread.sleep(2000)
        setTheme(R.style.AppTheme)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        txtNombreApp.text = getString(R.string.title)
        txtNombreApp.typeface = Typeface.createFromAsset(assets,"fonts/fuente3.ttf")

        //Evento OnClick
        btnRegistrar.setOnClickListener {

            //Creación de variables para obtener los datos de las cajas de texto
            val nombre = "Nombre: " + tvNombreR.text.toString()
            val email = tvEmailR.text.toString()
            val edad = "Edad: " + tvEdadR.text.toString()

            if(nombre.isEmpty()){
                Toast.makeText(this,"Debe ingresar su nombre",Toast.LENGTH_LONG).show()
                return@setOnClickListener    // deberia deternese el codigo y vuelva a solicitar
            }
            if(email.isEmpty()){
                Toast.makeText(this,"Debe ingresar su email",Toast.LENGTH_LONG).show()
                return@setOnClickListener    // deberia deternese el codigo y vuelva a solicitar
            }
            if(edad.isEmpty()){
                Toast.makeText(this,"Debe ingresar su edad",Toast.LENGTH_LONG).show()
                return@setOnClickListener    // deberia deternese el codigo y vuelva a solicitar
            }
            if(!chkTerminos.isChecked){
                Toast.makeText(this,"Debe aceptar los terminos",Toast.LENGTH_LONG).show()
                return@setOnClickListener    // deberia deternese el codigo y vuelva a solicitar
            }
            if(!rdbFemenino.isChecked && !rdbMasculino.isChecked){
                Toast.makeText(this,"Falta elejir el sexo",Toast.LENGTH_LONG).show()
                return@setOnClickListener    // deberia deternese el codigo y vuelva a solicitar
            }

            val bundle = Bundle()
            bundle.apply {
                putString("nombre",nombre)
                putString("edad",edad)
            }

            tvNombreR.text?.clear()
            tvEmailR.text?.clear()
            tvEdadR.text?.clear()
            chkTerminos.isChecked = false
            rdbFemenino.isChecked = false
            rdbMasculino.isChecked = false

            val intent = Intent(this,Fabula1::class.java).apply {
                putExtras(bundle)
                finish()
            }
            startActivity(intent)
        }

        tvAcercaDe.setOnClickListener {
            acercaDeDialog()
        }

    }

    private fun acercaDeDialog() {

        val builder: AlertDialog.Builder? = this.let {
            AlertDialog.Builder(it)
        }

        // Para poner el layaout perzonalizado en el dialog
        var view: View = layoutInflater.inflate(R.layout.acercade_dialog, null)
        var moraleja: LottieAnimationView = view.findViewById(R.id.animation_view)

        builder?.setCancelable(false)
        builder?.setView(view)

        moraleja.repeatCount = 1
        moraleja.playAnimation()

        builder?.apply {
            setPositiveButton("Aceptar",
                DialogInterface.OnClickListener { dialog, id ->
                    
                })
        }

        val dialog: AlertDialog? = builder?.create()

        dialog?.show()
    }
}
