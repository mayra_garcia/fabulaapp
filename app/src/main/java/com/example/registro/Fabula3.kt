package com.example.registro

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_fabula3.*

class Fabula3 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fabula3)

        val bundle : Bundle? = intent.extras

        bundle?.let {bundleLibriDeNull ->
            val nombre = bundleLibriDeNull.getString("nombre","Desconocido")
            val edad = bundleLibriDeNull.getString("edad","0")

            tvNombre3.text =  nombre
            tvEdad3.text =  edad
        }

        btnAvanzar3.setOnClickListener {

            //Pasar datos de la activity Fabula3 a la activity Fabula4
            val nombre = tvNombre3.text.toString()
            val edad = tvEdad3.text.toString()

            val bundle = Bundle()
            bundle.apply {
                putString("nombre",nombre)
                putString("edad",edad)
            }

            tvNombre3.text = ""
            tvEdad3.text = ""

            val intent = Intent(this,Fabula4::class.java).apply {
                putExtras(bundle)
                finish()
            }
            startActivity(intent)
        }

        btnRegresar3.setOnClickListener {

            //Pasar datos de la activity Fabula3 a la activity Fabula2
            val nombre = tvNombre3.text.toString()
            val edad = tvEdad3.text.toString()

            val bundle = Bundle()
            bundle.apply {
                putString("nombre",nombre)
                putString("edad",edad)
            }

            tvNombre3.text = ""
            tvEdad3.text = ""

            val intent = Intent(this,Fabula2::class.java).apply {
                putExtras(bundle)
                finish()
            }
            startActivity(intent)
        }
    }
}
